package sample;

import java.net.URL;
import java.util.ResourceBundle;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class Controller {
    public ImageView img;


    public TextField text1;
    public TextField text2;
    public TextField text3;
    public TextField text4;
    public TableView table1;
    public TableColumn firstName;
    public TableColumn lastName;
    public TableView table2;
    public Button btn;

    @FXML
    private TableView tableView;

    @FXML
    private Button btnsend;

    static ObservableList data =
            FXCollections.observableArrayList(
                    new Person("Jacob", "Smith"),
                    new Person("Isabella", "Johnson"),
                    new Person("Ethan", "Williams"),
                    new Person("Emma", "Jones"),
                    new Person("Michael", "Brown"));;



    public void initialize(URL arg0, ResourceBundle arg1) {

    }

    public void onbtn(ActionEvent actionEvent) {

        table1.setItems(data) ;
        table1.getColumns().addAll(firstName, lastName);
    }
}
